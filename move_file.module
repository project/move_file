<?php

/**
 * @file
 * Contains functions move_file_help() and move_file_node_presave().
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 *
 * @inheritdoc
 */
function move_file_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.move_file':
      $text = file_get_contents(__DIR__ . '/README.md');
      if (!Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . Html::escape($text) . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = Drupal::service('plugin.manager.filter');
        $settings = Drupal::configFactory()
          ->get('markdown.settings')
          ->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;
}

/**
 * Implements hook_node_insert().
 *
 * Move the files once a new node was stored.
 *
 * @inheritdoc
 */
function move_file_node_insert(EntityInterface $node) {
  if (\Drupal::service('move_file.move')->configsAreSet()) {
    \Drupal::service('move_file.move')->move($node);
  }
}

/**
 * Implements hook_node_update().
 *
 * Move the files once an existing node was updated.
 *
 * @inheritdoc
 */
function move_file_node_update(EntityInterface $node) {
  if (\Drupal::service('move_file.move')->configsAreSet()) {
    \Drupal::service('move_file.move')->move($node);
  }
}
