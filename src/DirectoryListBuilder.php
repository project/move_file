<?php

namespace Drupal\move_file;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Site\Settings;

/**
 * Defines a class to build a list of File Move directory entities.
 */
class DirectoryListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function render() {
    // Prepare the information render array.
    $information = [];
    $information['margin'] = [
      '#markup' => '<p />',
    ];

    // Check the private file system path.
    $private_file_system_path = Settings::get('file_private_path');
    if (!$private_file_system_path) {
      \Drupal::messenger()->addWarning($this->t(
        'Your private file system path is not set.'
      ));
    }
    else {
      $markup = '<p>' .
        $this->t(
          'Your private file system path is set to %path.',
          ['%path' => $private_file_system_path]
        ) .
        '</p>';
      $information['private_file_system_path'] = [
        '#markup' => $markup,
      ];
    }
    // Return the render array.
    return $information + parent::render();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    // Prepare the table header.
    $header = [];
    $header['id'] = $this->t('Id');
    $header['private'] = $this->t('Private file system');
    $header['path'] = $this->t('Directory path');
    $header['term_id'] = $this->t('Term ID');

    // Return the table header.
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $move_file_directory = $entity;

    // Prepare the table row for the directory.
    $row = [];
    $row['id'] = $move_file_directory->id();
    $row['private'] = $move_file_directory->private;
    $row['path'] = $move_file_directory->path;
    $row['term_id'] = $move_file_directory->term_id;

    // Return the table row.
    return $row + parent::buildRow($move_file_directory);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    $move_file_directory = $entity;

    // Prepare the table row operations for the directory.
    $operations = parent::getDefaultOperations($move_file_directory);
    if ($move_file_directory->hasLinkTemplate('edit')) {
      $operations['edit'] = [
        'title' => $this->t('Edit directory'),
        'weight' => 20,
        'url' => $move_file_directory->toUrl('edit'),
      ];
    }
    if ($move_file_directory->hasLinkTemplate('delete')) {
      $operations['delete'] = [
        'title' => $this->t('Delete directory'),
        'weight' => 40,
        'url' => $move_file_directory->toUrl('delete'),
      ];
    }

    // Return the table row operations.
    return $operations;
  }

}
