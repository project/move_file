<?php

namespace Drupal\move_file\Services;

use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\move_file\Entity\DirectoryEntity;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Move files referenced by a node.
 *
 * @package Drupal\move_file
 */
class MoveFileService {

  /**
   * Configurations.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * An entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   */
  public function __construct(ConfigFactory $config_factory, LoggerChannelFactoryInterface $logger_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->config = $config_factory->get('move_file.settings');
    $this->logger = $logger_factory->get('move_file');
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('logger.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Tests if all necessary configs and settings are set.
   *
   * @return bool
   *   Returns true, if the required configurations are set.
   */
  public function configsAreSet() {
    $configs_are_set = TRUE;

    if (
      NULL === $this->config->get('content_types') ||
      empty($this->config->get('content_types')) ||
      NULL === $this->config->get('vocabulary')
    ) {
      $configs_are_set = FALSE;
    }

    return $configs_are_set;
  }

  /**
   * Move files referenced by a node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The files referenced by this node will be moved.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function move(Node $node) {
    // Get activated content types from the config object.
    $activated_content_types = [];
    $activated_content_settings = [];
    foreach ($this->config->get('content_types') as $content_type_name => $content_type_fields) {
      $activated_content_types[] = $content_type_name;
      $activated_content_settings[$content_type_name] = $content_type_fields;
    }

    $node_type_name = $node->getType();
    if (in_array($node_type_name, $activated_content_types)) {

      // Get all selected taxonomy term ids.
      $vocabulary_field = $activated_content_settings[$node_type_name]['vocabulary_field'];
      $selected_term_ids = [];
      foreach ($node->{$vocabulary_field} as $selected_term) {
        $selected_term_ids[] = $selected_term->getValue()['target_id'];
      }

      // Get target directory.
      $target_directory = FALSE;
      foreach (DirectoryEntity::loadMultiple() as $directory) {
        if (in_array($directory->term_id, $selected_term_ids)) {
          $target_directory = $directory;
          break 1;
        }
      }

      // Define the target path.
      $file_target_path = FALSE;
      if ($target_directory) {
        // Get the directory entity.
        $file_scheme = $target_directory->private ? 'private' : 'public';
        $file_target_path = $file_scheme . ':/' . $target_directory->path . '/';
      }

      // Move all files from all activated fields to the target path.
      if ($file_target_path) {
        foreach ($activated_content_settings[$node_type_name]['file_field'] as $activated_file_field) {
          foreach ($node->get($activated_file_field) as $fileRef) {
            $fileId = $fileRef->getValue()['target_id'];
            $file = $this->entityTypeManager->getStorage('file')->load($fileId);
            $file_target_uri = $file_target_path . $file->getFilename();
            // The file should be moved only if the target URI is different from
            // the recent URI.
            if ($file_target_uri != $file->getFileUri()) {
              if (\Drupal::service('file.repository')->move($file, $file_target_uri)) {
                $this->logger->notice('The file "' . $file->getFilename() . '" has been moved to the path: ' . $file_target_uri);
              }
              else {
                $this->logger->error('The file "' . $file->getFilename() . '" could not be moved to the path: ' . $file_target_uri);
              }
            }
          }
        }
      }
    }
  }

}
