<?php

namespace Drupal\move_file\Tests\Form;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests the settings form.
 *
 * @group media
 * @group move_file
 */
class MoveFileContentTypesFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['move_file'];

  /**
   * Default theme.
   *
   * See: https://www.drupal.org/node/3083055
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Use the Standard profile.
   *
   * @var string
   *
   * @see \Drupal\simpletest\Tests\InstallationProfileModuleTestsTest
   * @see \Drupal\drupal_system_listing_compatible_test\Tests\SystemListingCompatibleTest
   */
  protected $profile = 'standard';

  /**
   * The role anonymous user.
   *
   * @var \Drupal\user\Entity\Role
   */
  private $guestRole;

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  private $adminPermissions;

  /**
   * Permissions to grant guest user.
   *
   * @var array
   */
  private $guestPermissions;

  /**
   * An user with administration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  private $adminUser;

  /**
   * An user with guest permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  private $guestUser;

  /**
   * Perform any initial set up tasks that run before every test method.
   *
   * Info to administrator permissions:
   * http://drupal.stackexchange.com/q/233416/72107
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp(): void {
    parent::setUp();

    $this->guestRole = Role::load('anonymous');
    $this->guestPermissions = $this->guestRole->getPermissions();
    $this->guestUser = $this->drupalCreateUser($this->guestPermissions);

    $this->adminPermissions = array_keys(\Drupal::service(
      'user.permissions')->getPermissions()
    );
    $this->adminUser = $this->drupalCreateUser($this->adminPermissions);
  }

  /**
   * Test the settings form.
   *
   * Test, that the '/admin/config/media/move-file/content-types' path returns
   * the right content and can be saved.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testSettingsFormAsAdmin() {

    $this->drupalLogin($this->adminUser);

    // Test the empty form.
    $this->drupalGet('/admin/config/media/move-file/content-types');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('Content types');

    $this->assertSession()->fieldExists(
      'content_types_list[article]'
    );
    $this->assertSession()->checkboxNotChecked(
      'edit-content-types-list-article'
    );

    $this->assertSession()->optionExists(
      'edit-settings-article-vocabulary-field',
      'field_tags'
    );
    $this->assertSession()->optionNotExists(
      'edit-settings-article-vocabulary-field',
      'field_tests'
    );

    $this->assertSession()->checkboxNotChecked(
      'edit-settings-article-file-field-field-image'
    );

    $this->assertSession()->fieldNotExists('test_field');

    // Test saving the form.
    $edit = [
      'content_types_list[article]' => 'article',
      'settings[article][vocabulary_field]' => 'field_tags',
      'settings[article][file_field][field_image]' => 'field_image',
    ];
    $this->submitForm($edit, t('Save configuration'));

    $this->assertSession()->pageTextContains(
      'The configuration options have been saved.'
    );

    // Test the updated the form.
    $this->assertSession()->checkboxChecked(
      'edit-content-types-list-article'
    );

    $this->assertSession()->fieldValueEquals(
      'edit-settings-article-vocabulary-field',
      'field_tags'
    );

    $this->assertSession()->checkboxChecked(
      'edit-settings-article-file-field-field-image'
    );

    $this->assertSession()->fieldNotExists('test_field');

  }

  /**
   * Test settings as guest.
   *
   * Tests that the '/admin/config/media/move-file/content-types' path is not
   * accessible for guests.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testSettingsFormAsGuest() {
    $this->drupalLogin($this->guestUser);

    $this->drupalGet('/admin/config/media/move-file/content-types');
    $this->assertSession()->statusCodeEquals(403);
  }

}
