<?php

namespace Drupal\move_file\Tests\Form;

use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests the settings form.
 *
 * @group media
 * @group move_file
 */
class MoveFileCreateConfigEntityTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['move_file'];

  /**
   * Default theme.
   *
   * See: https://www.drupal.org/node/3083055
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Use the Standard profile.
   *
   * @var string
   *
   * @see \Drupal\simpletest\Tests\InstallationProfileModuleTestsTest
   * @see \Drupal\drupal_system_listing_compatible_test\Tests\SystemListingCompatibleTest
   */
  protected $profile = 'standard';

  /**
   * The role anonymous user.
   *
   * @var \Drupal\user\Entity\Role
   */
  private $guestRole;

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  private $adminPermissions;

  /**
   * Permissions to grant guest user.
   *
   * @var array
   */
  private $guestPermissions;

  /**
   * An user with administration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  private $adminUser;

  /**
   * An user with guest permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  private $guestUser;

  /**
   * A taxonomy term.
   *
   * @var \Drupal\taxonomy\Entity\Term
   */
  private $term;

  /**
   * Perform any initial set up tasks that run before every test method.
   *
   * Info to administrator permissions:
   * http://drupal.stackexchange.com/q/233416/72107
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp(): void {
    parent::setUp();

    $this->guestRole = Role::load('anonymous');
    $this->guestPermissions = $this->guestRole->getPermissions();
    $this->guestUser = $this->drupalCreateUser($this->guestPermissions);

    $this->adminPermissions = array_keys(\Drupal::service(
      'user.permissions')->getPermissions()
    );
    $this->adminUser = $this->drupalCreateUser($this->adminPermissions);

    // Set configs.
    $config = \Drupal::configFactory()->getEditable('move_file.settings');
    $config->set('vocabulary', 'tags');
    $content_types = [
      "article" => [
        "vocabulary_field" => "field_tags",
        "file_field" => [
          "field_image",
        ],
      ],
    ];
    $config->set('content_types', $content_types);
    $config->save(TRUE);

    // Create term.
    $this->term = Term::create([
      'name' => 'aaa',
      'vid' => 'tags',
    ]);
    $this->term->save();
  }

  /**
   * Test the settings form.
   *
   * Test, that the '/admin/config/media/move-file/directories' path returns
   * the right content.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testSettingsFormAsAdmin() {

    $this->drupalLogin($this->adminUser);

    // Test the empty form.
    $this->drupalGet('/admin/config/media/move-file/directories');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(
      'Your private file system path is set to'
    );
    $this->assertSession()->pageTextContains(
      'There are no move file directory entities yet.'
    );

    $this->assertSession()->fieldNotExists('test_field');

    // Test the add form.
    $this->drupalGet('/admin/config/media/move-file/directories/add');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains(
      'Add directory'
    );

    $this->assertSession()->checkboxChecked(
      'private'
    );

    $this->assertSession()->fieldExists(
      'path'
    );
    $this->assertSession()->fieldValueEquals(
      'path',
      ''
    );

    $this->assertSession()->fieldExists(
      'term_id'
    );
    $this->assertSession()->fieldValueEquals(
      'term_id',
      ''
    );
    $this->assertSession()->optionExists(
      'term_id',
      '- Select -'
    );
    $this->assertSession()->optionExists(
      'term_id',
      'aaa'
    );
    $this->assertSession()->optionNotExists(
      'term_id',
      'tests'
    );

    $this->assertSession()->fieldNotExists('test_field');

    // Test saving the form.
    $edit = [
      'path' => '/aaa',
      'id' => '_aaa',
      'term_id' => $this->term->id(),
    ];
    $this->submitForm($edit, t('Save'));

    $this->assertSession()->pageTextContains(
      'The directory /aaa was saved successfully.'
    );

  }

  /**
   * Test settings as guest.
   *
   * Tests that the '/admin/config/media/move-file/directories' path is not
   * accessible for guests.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testSettingsFormAsGuest() {
    $this->drupalLogin($this->guestUser);

    $this->drupalGet('/admin/config/media/move-file/directories');
    $this->assertSession()->statusCodeEquals(403);
  }

}
