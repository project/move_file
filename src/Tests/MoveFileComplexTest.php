<?php

namespace Drupal\move_file\Tests;

use Drupal\file\Entity\File;
use Drupal\move_file\Entity\DirectoryEntity;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the settings form.
 *
 * @group media
 * @group move_file
 */
class MoveFileComplexTest extends BrowserTestBase {

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  private $adminPermissions;

  /**
   * An user with administration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  private $adminUser;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['move_file'];

  /**
   * Default theme.
   *
   * See: https://www.drupal.org/node/3083055
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Use the Standard profile.
   *
   * @var string
   *
   * @see \Drupal\simpletest\Tests\InstallationProfileModuleTestsTest
   * @see \Drupal\drupal_system_listing_compatible_test\Tests\SystemListingCompatibleTest
   */
  protected $profile = 'standard';

  /**
   * The first taxonomy term.
   *
   * @var \Drupal\taxonomy\Entity\Term
   */
  private $termA;

  /**
   * The second taxonomy term.
   *
   * @var \Drupal\taxonomy\Entity\Term
   */
  private $termB;

  /**
   * File used for testing.
   *
   * @var \Drupal\file\Entity\File
   */
  private $fileToMove;

  /**
   * Article.
   *
   * @var \Drupal\node\Entity\Node
   */
  private $article;

  /**
   * Perform any initial set up tasks that run before every test method.
   *
   * Create two tags. Create two directory entities. Activate the article
   * content type. Create an article with an image. Info to administrator
   * permissions:
   * http://drupal.stackexchange.com/q/233416/72107
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp(): void {
    parent::setUp();

    // Set up our custom test config.
    $config = \Drupal::configFactory()->getEditable('move_file.settings');
    $config->set('vocabulary', 'tags');
    $content_types = [
      "article" => [
        "vocabulary_field" => "field_tags",
        "file_field" => [
          "field_image",
        ],
      ],
    ];
    $config->set('content_types', $content_types);
    $config->save(TRUE);

    $this->adminPermissions = array_keys(\Drupal::service(
      'user.permissions')->getPermissions()
    );
    $this->adminUser = $this->drupalCreateUser($this->adminPermissions);

    // Create terms.
    $this->termA = Term::create([
      'name' => 'aaa',
      'vid' => 'tags',
    ]);
    $this->termA->save();
    $this->termB = Term::create([
      'name' => 'bbb',
      'vid' => 'tags',
    ]);
    $this->termB->save();

    // Copy the test file to the public folder.
    copy('./modules/contrib/move_file/src/Tests/files/file-to-move.png', './sites/default/files/file-to-move.png');
    $this->fileToMove = system_retrieve_file('http://localhost/sites/default/files/file-to-move.png', 'public://file-to-move.png', TRUE);

    // Create article.
    $this->article = $this->drupalCreateNode([
      'type' => 'article',
      'title' => 'Test',
      'field_image' => [
        $this->fileToMove->id(),
      ],
    ]);

    // Create config entities.
    $entityA = DirectoryEntity::create([
      'id' => 'aaa',
      'private' => FALSE,
      'path' => '/aaa',
      'term_id' => $this->termA->id(),
    ]);
    $entityA->save();
    $entityB = DirectoryEntity::create([
      'id' => 'bbb',
      'private' => FALSE,
      'path' => '/bbb',
      'term_id' => $this->termB->id(),
    ]);
    $entityB->save();

  }

  /**
   * Test the module.
   *
   * Test the complete module. Update the article and test, if the image is
   * moved.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testMovingTheFile() {
    $this->drupalLogin($this->adminUser);

    // Add directory config entities.
    $this->drupalGet('/admin/config/media/move-file/directories/add');
    $edit = [
      'private' => 0,
      'path' => '/aaa',
      'id' => '_aaa',
      'term_id' => $this->termA->id(),
    ];
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains(
      'The directory /aaa was saved successfully.'
    );
    $this->drupalGet('/admin/config/media/move-file/directories/add');
    $edit = [
      'private' => 0,
      'path' => '/bbb',
      'id' => '_bbb',
      'term_id' => $this->termB->id(),
    ];
    $this->submitForm($edit, t('Save'));
    $this->assertSession()->pageTextContains(
      'The directory /bbb was saved successfully.'
    );

    $this->assertEquals(1, $this->article->get('field_image')->getValue()[0]['target_id'], 'ID of the file entity.');

    $file_entity_id = $this->article->field_image->getValue()[0]['target_id'];
    $file_entity = File::load($file_entity_id);
    $file_uri = $file_entity->getFileUri();
    // For debugging: 'fwrite(STDOUT, "File URI 1: {$file_uri}\n");'.
    $this->assertEquals('public://file-to-move.png', $file_uri, 'Test, if the file is saved in the public folder.');

    $this->article->field_tags = $this->termA->id();
    $this->article->save();
    $file_entity = File::load($file_entity_id);
    $file_uri = $file_entity->getFileUri();
    // For debugging: 'fwrite(STDOUT, "File URI 2: {$file_uri}\n")'.
    $this->assertEquals('public://aaa/file-to-move.png', $file_uri, 'Test, if the file is saved in the aaa folder.');

    $this->article->field_tags = $this->termB->id();
    $this->article->save();
    $file_entity = File::load($file_entity_id);
    $file_uri = $file_entity->getFileUri();
    // For debugging: 'fwrite(STDOUT, "File URI 3: {$file_uri}\n");'.
    $this->assertEquals('public://bbb/file-to-move.png', $file_uri, 'Test, if the file is saved in the bbb folder.');
  }

}
