<?php

namespace Drupal\move_file\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Directory form for move_file.
 */
class DirectoryForm extends EntityForm {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a DirectoryForm.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(FileSystemInterface $file_system = NULL) {
    $this->fileSystem = $file_system;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $move_file_directory = $this->entity;

    // Prepare the form.
    $form = parent::form($form, $form_state);

    $form['private'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Private file system'),
      '#default_value' => $move_file_directory->private,
      '#description' => $this->t('If checked, private file system will be used. Otherwise will be used public file system.'),
    ];

    $form['path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path'),
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
      '#default_value' => $move_file_directory->path,
      '#description' => $this->t('If the path in the file system does not exists, then will be the directory created.'),
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#disabled' => !$move_file_directory->isNew(),
      '#default_value' => $move_file_directory->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'source' => ['path'],
      ],
    ];

    $form['term_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Term'),
      '#required' => TRUE,
      '#default_value' => $move_file_directory->term_id,
      '#options' => $this->getTermsList(),
      '#description' => $this->t('If the term is selected, then the files will be moved to the specified directory, when a node is saved.'),
    ];

    // Return the form.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Retrieve $path (which, being required, is surely not blank).
    $path = $form_state->getValue('path');

    // Perform slash validation:
    if (0 < mb_strlen($path)) {
      $first_character = mb_substr($path, 0, 1);
      $last_character = mb_substr($path, -1, 1);
      // ...there must be a leading slash.
      if (('/' !== $first_character) && ('\\' !== $first_character)) {
        $form_state->setErrorByName('path', $this->t('You must add a leading slash.'));
      }
      if (1 < mb_strlen($path)) {
        // ...there cannot be multiple consecutive slashes.
        if ((FALSE !== mb_strpos($path, '//')) || (FALSE !== mb_strpos($path, '\\\\'))) {
          $form_state->setErrorByName('path', $this->t('You cannot use multiple consecutive slashes.'));
        }
        // ...there cannot be trailing slashes.
        if (('/' === $last_character) || ('\\' === $last_character)) {
          $form_state->setErrorByName('path', $this->t('You cannot use trailing slashes.'));
        }
      }
    }

    // Retrieve $termId (which, being required, is surely not blank).
    $termId = $form_state->getValue('term_id');

    // Perform integer validation:
    if (0 < mb_strlen($termId) && !(is_int($termId) || ctype_digit($termId))) {
      $form_state->setErrorByName('term_id', $this->t('The term ID have to be an integer.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $move_file_directory = $this->entity;
    // Save the directory and display the status message.
    try {
      $move_file_directory->save();

      // Create the directory if it does not exist.
      $directory_path = $move_file_directory->private ? 'private:/' . $move_file_directory->path : 'public:/' . $move_file_directory->path;
      $this->fileSystem->prepareDirectory($directory_path, FileSystemInterface::CREATE_DIRECTORY);

      $this->logger('move_file')->info('The directory %path was saved successfully.', ['%path' => $move_file_directory->path]);
      $this->messenger()->addStatus($this->t('The directory %path was saved successfully.', ['%path' => $move_file_directory->path]));
    }
    catch (EntityStorageException $exception) {
      $this->logger('move_file')->error('The directory %path was not saved.', ['%path' => $move_file_directory->path]);
      $this->messenger()->addError($this->t('The directory %path was not saved.', ['%path' => $move_file_directory->path]));
    }
    // Redirect to the proper url.
    $form_state->setRedirect('entity.move_file_directory');
  }

  /**
   * Checks if the directory exists.
   *
   * @param string $id
   *   Machine name (ID) of the Move File directory.
   *
   * @return bool
   *   Returns true, if the directory exist.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists($id) {
    $storage = $this->entityTypeManager->getStorage('move_file_directory');
    $move_file_directory = $storage->getQuery()->condition('id', $id)->execute();
    return (bool) $move_file_directory;
  }

  /**
   * Retrieves an array of taxonomy terms.
   *
   * @return array
   *   Taxonomy terms.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTermsList() {
    // Retrieve the configuration object.
    $config = $this->config('move_file.settings');

    // Create an array with a list of the terms.
    $vocabulary_id = $config->get('vocabulary');
    $terms_list = [];
    if (isset($vocabulary_id)) {
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($vocabulary_id);
      foreach ($terms as $term) {
        $terms_list[$term->tid] = $term->name;
      }
    }

    return $terms_list;
  }

}
