<?php

namespace Drupal\move_file\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configures Move File settings for this site.
 */
class MoveFileContentTypesForm extends ConfigFormBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity field manager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Constructs a new DefaultSelection object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($config_factory);

    $this->entityTypeManager = $entity_type_manager;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'move_file_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config_content_types = $this->config('move_file.settings')->get('content_types');

    // Get default values from the config object.
    $selected_content_types = [];
    if ($config_content_types) {
      foreach ($config_content_types as $content_type_name => $content_type_fields) {
        $selected_content_types[$content_type_name] = $content_type_name;
      }
    }
    $content_types = $this->getContentTypesList();

    $form['content_types_list'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Content types'),
      '#options' => $content_types,
      '#default_value' => $selected_content_types,
      '#description' => $this->t('Only in the selected content types will be the Move File module activated. You have to configure the selected content types below.'),
    ];

    $form['settings'] = ['#tree' => TRUE];

    foreach ($content_types as $content_type_id => $content_type_label) {

      // Get default values for the vocabulary and file fields.
      $vocabulary_field = isset($selected_content_types[$content_type_id]) ? $config_content_types[$content_type_id]['vocabulary_field'] : NULL;
      $file_field = isset($selected_content_types[$content_type_id]) ? $config_content_types[$content_type_id]['file_field'] : [];

      $form['settings'][$content_type_id] = [
        '#title' => $content_type_label,
        '#markup' => '<hr><h3>' . $content_type_label . '</h3>',
        '#type' => 'container',
        '#states' => [
          'visible' => [
            ':input[name="content_types_list[' . $content_type_id . ']"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form['settings'][$content_type_id]['vocabulary_field'] = [
        '#type' => 'select',
        '#title' => $this->t('Vocabulary field'),
        '#options' => $this->getFieldsList($content_type_id, ['entity_reference']),
        '#empty_option' => '---',
        '#default_value' => $vocabulary_field,
        '#description' => $this->t('Select the field with the vocabulary.'),
      ];
      $form['settings'][$content_type_id]['file_field'] = [
        '#type' => 'checkboxes',
        '#title' => $this->t('File fields'),
        '#options' => $this->getFieldsList($content_type_id, ['image', 'file']),
        '#default_value' => $file_field,
        '#description' => $this->t('Files from the selected fields will be moved to the specified directories.'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $content_types_list = $form_state->getValues()['content_types_list'];
    $settings = $form_state->getValues()['settings'];

    $content_types = [];
    foreach ($settings as $content_type_name => $content_type_fields) {
      // Test if at least one file field is selected.
      $file_fields = [];
      foreach ($content_type_fields['file_field'] as $file_field) {
        if ($file_field) {
          $file_fields[] = $file_field;
        }
      }
      // Add content type to the list of activated content types.
      if (
        $content_types_list[$content_type_name] &&
        strlen($content_type_fields['vocabulary_field']) > 0 &&
        !empty($file_fields)
      ) {
        $content_types[$content_type_name] = [
          'vocabulary_field' => $content_type_fields['vocabulary_field'],
          'file_field' => $file_fields,
        ];
      }
    }

    $this->config('move_file.settings')
      ->set('content_types', $content_types)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['move_file.settings'];
  }

  /**
   * Retrieves an array of content types.
   *
   * @return array
   *   List of content types.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getContentTypesList() {
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $content_types_list = [];
    foreach ($content_types as $content_type) {
      $content_types_list[$content_type->id()] = $content_type->label();
    }

    return $content_types_list;
  }

  /**
   * Retrieves an array of content type fields.
   *
   * @param string $content_type_id
   *   ID of the content type.
   * @param array $field_types
   *   Field types, which should be displayed.
   *
   * @return array
   *   List of fields.
   */
  public function getFieldsList($content_type_id, array $field_types = ['entity_reference']) {
    $content_type_field_definitions = $this->entityFieldManager->getFieldDefinitions('node', $content_type_id);

    $field_list = [];
    foreach ($content_type_field_definitions as $field_name => $field_definition) {
      // Only fields of specified types will be displayed.
      if (
        is_a($field_definition, 'Drupal\field\Entity\FieldConfig') &&
        in_array($field_definition->get('field_type'), $field_types)
      ) {
        $field_list[$field_name] = $field_name;
      }
    }

    return $field_list;
  }

}
