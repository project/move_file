<?php

namespace Drupal\move_file\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Directory delete form for private_files_download_permission.
 */
class DirectoryDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $move_file_directory = $this->entity;
    return $this->t('Are you sure you want to delete the directory %path from the control list?', ['%path' => $move_file_directory->path]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.move_file_directory');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $move_file_directory = $this->entity;
    // Delete the directory and display the status message.
    try {
      $move_file_directory->delete();
      $this->logger('move_file')->info('The directory %path was deleted successfully.', ['%path' => $move_file_directory->path]);
      \Drupal::messenger()->addStatus($this->t('The directory %path was deleted successfully.', ['%path' => $move_file_directory->path]));
    }
    catch (EntityStorageException $exception) {
      $this->logger('move_file')->error('The directory %path was not deleted.', ['%path' => $move_file_directory->path]);
      \Drupal::messenger()->addError($this->t('The directory %path was not deleted.', ['%path' => $move_file_directory->path]));
    }
    // Set form redirection.
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
