<?php

namespace Drupal\move_file\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configures Move File settings for this site.
 */
class MoveFileSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'move_file_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('move_file.settings');

    // Create a list of all taxonomies.
    $vocabularyList = \Drupal::entityQuery('taxonomy_vocabulary')->accessCheck(FALSE)->execute();

    $form['info_box'] = [
      '#type' => 'details',
      '#title' => $this->t('Infobox'),
      '#description' => $this->t('Select the vocabulary, in which can be the taxonomy terms found, which are related to the Move File directories.'),
      '#open' => TRUE,
    ];

    $form['move_file_vocabulary'] = [
      '#type' => 'select',
      '#title' => $this->t('Vocabulary'),
      '#description' => $this->t('Vocabulary, in which can be found the related terms to the Move File directories.'),
      '#default_value' => $config->get('vocabulary'),
      '#empty_option' => '---',
      '#required' => TRUE,
      '#options' => $vocabularyList,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Test the fields for the minimum length.
    if ($form_state->isValueEmpty('move_file_vocabulary')) {
      $form_state->setErrorByName('move_file_vocabulary',
        $this->t('Without selecting the vocabulary does the module not work.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('move_file.settings')
      ->set('vocabulary', $form_state->getValue('move_file_vocabulary'))
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['move_file.settings'];
  }

}
