<?php

namespace Drupal\move_file\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the Move File directory entity.
 *
 * @ConfigEntityType(
 *   id = "move_file_directory",
 *   label = @Translation("Move File directory"),
 *   module = "move_file",
 *   config_prefix = "move_file_directory",
 *   handlers = {
 *     "list_builder" = "Drupal\move_file\DirectoryListBuilder",
 *     "form" = {
 *       "add" = "Drupal\move_file\Form\DirectoryForm",
 *       "edit" = "Drupal\move_file\Form\DirectoryForm",
 *       "delete" = "Drupal\move_file\Form\DirectoryDeleteForm",
 *     }
 *   },
 *   admin_permission = "administer move_file",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   config_export = {
 *     "id",
 *     "private",
 *     "path",
 *     "term_id"
 *   },
 *   links = {
 *     "edit" = "/admin/config/media/move-file/{move_file_directory}",
 *     "delete" = "/admin/config/media/move-file/{move_file_directory}/delete",
 *   },
 * )
 */
class DirectoryEntity extends ConfigEntityBase implements DirectoryEntityInterface {

  /**
   * ID of the Directory Entity.
   *
   * @var string
   */
  public $id = NULL;

  /**
   * Is the directory private (true) or public (false).
   *
   * @var bool
   */
  public $private = TRUE;

  /**
   * Path of the directory in the private/public file system.
   *
   * @var string
   */
  public $path = NULL;

  /**
   * Id of the taxonomy term related to the directory.
   *
   * @var int
   */
  public $term_id = NULL;

}
