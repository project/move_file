<?php

namespace Drupal\move_file\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Interface for Move File directories.
 */
interface DirectoryEntityInterface extends ConfigEntityInterface {
}
