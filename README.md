CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

The module enable to automatically move files to specified directories. Together
with the module [Private files download permission](
https://www.drupal.org/project/private_files_download_permission) it is possible
to dynamically change the download permissions of files. The files are moved,
when a node is saved, which contains references to the files.

You can use the module e.g. for changing file permissions dynamically for
several groups of users. When everything is set, then you just choose another
taxonomy term in a node and the file(s) will be moved to the appropriate folder.

More info:

 * For a full description of the module, visit
   [the project page](https://www.drupal.org/project/move_file).

 * To submit bug reports and feature suggestions, or to track changes, visit
   [the issue page](https://www.drupal.org/project/issues/move_file).

REQUIREMENTS
------------

There are no required modules.

RECOMMENDED MODULES
-------------------

 * [Private files download permission](
   https://www.drupal.org/project/private_files_download_permission)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See the
   [docs](https://www.drupal.org/documentation/install/modules-themes/modules-8)
   for further information.

CONFIGURATION
-------------

At first you need to have a vocabulary with taxonomy terms, which will be used
to specify where should be the files moved. Then you need a content type
containing a field referencing the vocabulary and a field with images or files.

The module has tree configuration pages.

 1. On the first page you select the vocabulary.

 2. On the second page you can define directories. In the directory form can be
 specified, if the directory is private or public, the directory path and the
 taxonomy term, which is related to the directory.

 3. On the third page you should activate the content types, in which you would
 like to move the files. In every activated content type have to be specified
 the field field with references to the vocabulary and field(s) with references
 to the files or images.

TROUBLESHOOTING
---------------

There are not any known issues. See:

 * [Issues](https://www.drupal.org/project/issues/move_file)
 * [Automated testing](https://www.drupal.org/node/3115871/qa)
 * [PAReview](
   https://pareview.sh/pareview/https-git.drupal.org-project-move_file.git)

FAQ
---

There are no answers or issues.

MAINTAINERS
-----------

Current maintainers:

 * [Antonín Slejška](https://www.drupal.org/u/anton%C3%ADn-slej%C5%A1ka)
